# RpAngRoadmap

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.



## Build
Take clone of this project 
Run `npm install` to build the project in clone directory. 
and then run `ng serve` in project directory.
and if you got an error of socialLoginmodule then update version by this command `ng update @angular/cli @angular/core --allow-dirty`



## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
