import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { RegistrationComponent } from './pages/registration/registration.component';
import { TableComponent } from './pages/table/table.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatRadioModule} from '@angular/material/radio';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { FileUploadComponent } from './pages/file-upload/file-upload.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DropzoneDirective } from './dropzone.directive';
import { TwoStepVerificationComponent } from './pages/two-step-verification/two-step-verification.component';
import { OtpVerificationComponent } from './pages/otp-verification/otp-verification.component';
import { NgOtpInputModule } from 'ng-otp-input';
import { GoogleLoginProvider, SocialAuthServiceConfig, SocialLoginModule } from '@abacritt/angularx-social-login';
import { LoginViaGmailComponent } from './pages/login-via-gmail/login-via-gmail.component';
import { SuccesspageComponent } from './pages/successpage/successpage.component';
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    RegistrationComponent,
    TableComponent,
    FileUploadComponent,
    DropzoneDirective,
    TwoStepVerificationComponent,
    OtpVerificationComponent,
    LoginViaGmailComponent,
    SuccesspageComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    BrowserAnimationsModule,
    MatRadioModule,
    MatSnackBarModule,
    MatDialogModule,
    NgOtpInputModule,
    SocialLoginModule
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers: [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider(
              '756693081594-qdiglnpduvpq6fr67nnes5q4p77l134h.apps.googleusercontent.com'
            ),
          },
          
        ],
        onError: (err: any) => {
          console.error(err);
        },
      } as SocialAuthServiceConfig,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
