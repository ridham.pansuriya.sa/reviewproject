import {  NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileUploadComponent } from '../file-upload/file-upload.component';
import { LoginViaGmailComponent } from '../login-via-gmail/login-via-gmail.component';
import { RegistrationComponent } from '../registration/registration.component';
import { SuccesspageComponent } from '../successpage/successpage.component';
import { TableComponent } from '../table/table.component';
import { TwoStepVerificationComponent } from '../two-step-verification/two-step-verification.component';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children:[
      {
        path: '',
        redirectTo: 'registration',
        pathMatch: 'full'
      },
      {
        path: 'registration', component: RegistrationComponent,
      },
      {
        path: 'table' , component:TableComponent
      },
      {
        path:'file-upload' , component:FileUploadComponent,
      },
      {
        path:'two-step', component:TwoStepVerificationComponent,
      },
      {
        path:'login', component:LoginViaGmailComponent,
      },{
        path:'success', component:SuccesspageComponent
      }
    
    
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
