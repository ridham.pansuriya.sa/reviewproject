import { Component, OnInit } from '@angular/core';

import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.css']
})
export class FileUploadComponent implements OnInit {

  allFiles: File[] = [];
  user:any;
  constructor( private userService : UserService) { }
  ngOnInit(): void {
    this.userService.castUser.subscribe(user => this.user = user);
    console.log('user',this.user);
    
  }
  droppedFiles(allFiles: File[]): void {
    const filesAmount = allFiles.length;    
    
    for (let i = 0; i < filesAmount; i++) {
      
      if(allFiles[i].type.includes('image') || allFiles[i].type == 'application/pdf' ){
        if(allFiles[i].size<1000000){
          
          const file = allFiles[i];
          this.allFiles.push(file);
          alert(`${allFiles[i].name}.fontcolor( "red" ) file is successfully uploaded`)
        }
        else{
          alert(` ${allFiles[i].name} \n file size is too big please enter less than 1mb file`)
          console.error()
        }
      }
      
      else{
        alert(`please upload a valid file (png , jpeg , jpg , pdf) \n${allFiles[i].name} is not a valid file `)
        
      }
      
    }
  }
  selectedfile(event:any){
    const file=event.target.files;   
    this.droppedFiles(file);
  }
  

}
