import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginViaGmailComponent } from './login-via-gmail.component';

describe('LoginViaGmailComponent', () => {
  let component: LoginViaGmailComponent;
  let fixture: ComponentFixture<LoginViaGmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginViaGmailComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginViaGmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
