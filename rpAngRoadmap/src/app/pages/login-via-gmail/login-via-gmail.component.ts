import { SocialAuthService } from '@abacritt/angularx-social-login';
import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login-via-gmail',
  templateUrl: './login-via-gmail.component.html',
})
export class LoginViaGmailComponent implements OnInit {
  user:any;
  constructor(private authService:SocialAuthService, private userService: UserService) { }
  logoutflag=false;
  ngOnInit(): void {
    
    this.authService.authState.subscribe((user) => {
      this.user = user;
      console.log(user);
      this.editedUser(user);
    });
   
  }

    editedUser(user:any){
      if(user?.name){
        this.logoutflag=true;
        this.userService.editUser(user);
      }
     
    }
    logout(){
      this.authService.signOut();
      this.logoutflag=false;
      this.userService.editUser({});
    }
}
