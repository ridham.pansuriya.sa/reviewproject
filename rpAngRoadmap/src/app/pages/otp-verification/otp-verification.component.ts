import { Component, EventEmitter,Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-otp-verification',
  templateUrl: './otp-verification.component.html',
  styleUrls: ['./otp-verification.component.css']
})
export class OtpVerificationComponent implements OnInit {

  sendedOtp:any;
  enteredOtp:any;
  successFlag=false;
  failflag=false;
  email:any
  value:any;
  chancesotp=false;
  count:any;
  @Output() back = new EventEmitter();


  constructor(private _router: Router) { }
  @Input() registrationData : any;
  ngOnInit(): void {
    this.count=4;
    console.log(this.registrationData);
    this.sendedOtp=this.registrationData[0].Otp;
    this.email=this.registrationData[0].email
  }
  onOtpChange(value:any){
   
    this.enteredOtp=value;
  }
  
  verifyOtp(){
    if(this.enteredOtp==this.sendedOtp){
      this.successFlag=true;
      this.navigateTosuccessPage();
    }
    else{
      this.count--;
      this.chancesotp=true;
      if(this.count==0){
        this.failflag=true;
        alert('you have reached max otp try now please verify and  reenter  your mail ')
        this.back.emit('true');

      }
    }
  }
  resendOtp(){
    this.value=Math.floor(Math.random()*100000+1);
    console.log('otp : ', this.value);
    this.sendedOtp=this.value;
  }

  backpage(){
    this.back.emit('true');

  }



  navigateTosuccessPage() {
    this._router.navigate(['/main/success'])
  }

}
