import { Component, OnInit } from '@angular/core';
import { AbstractControl, UntypedFormArray,  UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})


export class RegistrationComponent implements OnInit {
  registerForm: UntypedFormGroup;
  MainArray:any;
  valueforedit:any;
  editForm=false;
  isSubmited=false;

  ngOnInit(): void {
    const emailRegEx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$';
    this.MainArray=[]
    this.registerForm = new UntypedFormGroup({
      name: new UntypedFormControl('', [Validators.required]),
      phones: new UntypedFormArray([
        new UntypedFormGroup({
          contact : new UntypedFormControl('',[Validators.maxLength(10),Validators.required,Validators.minLength(10)])
        })
      ],[Validators.required]),
      email: new UntypedFormControl('',[Validators.required,Validators.email,Validators.pattern(emailRegEx)]),
      gender: new UntypedFormControl('',[Validators.required])
  });
  }

  addPhone(): void {
    (this.registerForm.get('phones') as UntypedFormArray).push(
      new UntypedFormGroup({
        contact : new UntypedFormControl('', [Validators.required,Validators.maxLength(10),Validators.minLength(10)])
      })
    );
  }

  removePhone(index:any) {
    (this.registerForm.get('phones') as UntypedFormArray).removeAt(index);
  }

  getPhonesFormControls(): AbstractControl[] {
    return (<UntypedFormArray> this.registerForm.get('phones')).controls    
  }
  get getphonesvalidation(): UntypedFormArray  {
    return (<UntypedFormArray> this.registerForm.get('phones'))  as UntypedFormArray  
  }

  edititem(item:any){
    this.editForm=true;
    this.valueforedit=item;
    let lengthOfPhone = (this.registerForm.get('phones') as UntypedFormArray).controls.length;
    for(let i=0;i<=lengthOfPhone;i++){      
      (this.registerForm.get('phones') as UntypedFormArray).removeAt(0)
     }
    let object = {
      name:item.name,
      email:item.email,
      gender:item.gender
    }
    this.registerForm.patchValue(object);
    item.edit=true;
    
      for(let i=0;i<item.phones.length;i++){
        (this.registerForm.get('phones') as UntypedFormArray).push(new UntypedFormGroup(
          {
              contact : new UntypedFormControl(item.phones[i].contact,[Validators.required,Validators.maxLength(10),Validators.minLength(10)])
          }
        ))
      }
  }
  
  onSubmit(){
    this.isSubmited=true;
    if(this.registerForm.valid){
      this.isSubmited=false;
      let value =this.registerForm.value;
      value.edit=false;
      this.MainArray.push(value);
      this.registerForm.reset();
      let lengthOfPhone = (this.registerForm.get('phones') as UntypedFormArray).controls.length;
      for(let i=1;i<lengthOfPhone;i++){      
        (this.registerForm.get('phones') as UntypedFormArray).removeAt(0)
       }
    }

    
  }
  save(){
    this.onSubmit();
    this.editForm=false;     
  }
  cancel(){
    this.editForm=false;
    this.valueforedit.edit=false;
    this.MainArray.push(this.valueforedit);
    this.registerForm.reset();
    this.isSubmited=false;
    this.registerForm.controls['name'].markAsUntouched();
    let lengthOfPhone = (this.registerForm.get('phones') as UntypedFormArray).controls.length;
      for(let i=1;i<lengthOfPhone;i++){      
        (this.registerForm.get('phones') as UntypedFormArray).removeAt(0)
       }
     console.log(this.registerForm,'ilfsh');

    }

}
