import { Component, Input, OnInit, Output ,EventEmitter } from '@angular/core';
import { AbstractControl, UntypedFormArray, UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  editflag=false;
  lengthOfPhone:any;
  constructor(private userService: UserService) { }
  editDataForm: UntypedFormGroup;
  array1:any;
  @Input() MainArray:any;
  @Output() edititem = new EventEmitter();

  user:any;
  ngOnInit(): void {
    this.userService.castUser.subscribe(user => this.user = user[0]);
    console.log('hiii',this.user);
    
    this.editDataForm = new UntypedFormGroup({
      name: new UntypedFormControl('', [Validators.required]),
      phones: new UntypedFormArray([]),
      email: new UntypedFormControl('',[Validators.required,Validators.email]),
      gender: new UntypedFormControl('',[Validators.required])
  });
  this.array1 = this.MainArray;

  }
  

  heading=['name','phones','email','gender']
  edit(item:any,index:any){
    this.editflag=true;
    item.edit=true;
      this.edititem.emit(item);
      this.delete(index);
  }
  
  save(item:any,i:any){
    item.edit=false;
    this.editflag=false;    
    this.array1[i]=this.editDataForm.value;
    for(let i=0;i<=this.lengthOfPhone;i++){      
      (this.editDataForm.get('phones') as UntypedFormArray).removeAt(0)
     }
    this.editDataForm.reset() ;
  }
  dontSave(item:any){
    
    for(let i=0;i<=this.lengthOfPhone;i++){      
      (this.editDataForm.get('phones') as UntypedFormArray).removeAt(0)
     }
    this.editDataForm.reset() ;
    item.edit=false;
    this.editflag=false;
  }
  
  delete(item:any){
    this.array1.splice(item, 1);   
    console.log(this.array1);
    
  }

  onSubmit(){

  }
  getPhonesFormControls(): AbstractControl[] {
    return (<UntypedFormArray> this.editDataForm.get('phones')).controls    
  }


}
