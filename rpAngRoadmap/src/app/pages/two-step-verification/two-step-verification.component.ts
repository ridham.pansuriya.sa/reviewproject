import { Component, OnInit } from '@angular/core';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-two-step-verification',
  templateUrl: './two-step-verification.component.html',
  styleUrls: ['./two-step-verification.component.css']
})
export class TwoStepVerificationComponent implements OnInit {

 
  submitForm=false;
  isSubmited=false;
  ArrayOfformVAlue:any=[];
  verificationForm:any;
  ngOnInit(): void {
    const emailRegEx = '^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$';
    const phoneRegex = '^[0-9]{10}$';
    this.verificationForm = new UntypedFormGroup({
      name: new UntypedFormControl('', [Validators.required]),
      phone: new UntypedFormControl('',[Validators.maxLength(10),Validators.required,Validators.minLength(10),Validators.pattern(phoneRegex)]),
      email: new UntypedFormControl('',[Validators.required,Validators.email,Validators.pattern(emailRegEx)]),
      gender: new UntypedFormControl('',[Validators.required])
  });
  }

  onSubmit(){
    
    this.isSubmited=true;
    this.ArrayOfformVAlue=[];
    if(this.verificationForm.valid){
      this.submitForm=true;
      let value:any =this.verificationForm.value;
      value.Otp=Math.floor(Math.random()*100000+1);
      this.ArrayOfformVAlue.push(value);
    }
  
    this.verificationForm.reset(this.verificationForm.value);
    
  }
  flagstatus(value:any){
    this.submitForm=false;
  }

}
